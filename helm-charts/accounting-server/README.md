### Install

```
helm install --name accounting-server-staging \
    --tiller-namespace accounting \
    --namespace accounting \
    --set NODE_ENV=staging \
    --set postgresql.postgresqlPassword=SuperSecretString \
    --set image.tag=edge \
    --set ingress.hosts={staging-accounting.castlecraft.in} \
    --set ingress.tls[0].secretName=accounting-castlecraft-in-staging-tls \
    --set ingress.tls[0].hosts={staging-accounting.castlecraft.in} \
    helm-charts/accounting-server
```

### Delete

```
helm del --tiller-namespace accounting --purge accounting-server-staging
```
