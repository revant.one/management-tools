#!/usr/bin/env bash
echo "Building Accounting Server Docs"
./node_modules/.bin/typedoc\
    --out public/api/accounting-server \
    packages/accounting-server/src \
    --name "Accounting Server"

echo "Building Accounting Client Docs"
./node_modules/.bin/typedoc \
    --out public/api/client \
    packages/client/src \
    --name "Accounting Client"
