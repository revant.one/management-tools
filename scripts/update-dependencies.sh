#!/usr/bin/env bash

# Update packages

# Root Dependencies
./node_modules/.bin/npm-check --update

# Accounting Server
./node_modules/.bin/npm-check --update packages/accounting-server

# Accounting Client
./node_modules/.bin/npm-check --update packages/client
