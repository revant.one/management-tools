#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_AS=$(helm ls -q accounting-server-staging --tiller-namespace accounting)
if [ "$CHECK_AS" = "accounting-server-staging" ]
then
    echo "Updating existing accounting-server-staging . . ."
    helm upgrade accounting-server-staging \
        --tiller-namespace accounting \
        --namespace accounting \
        --reuse-values \
        --recreate-pods \
        --wait \
        helm-charts/accounting-server
fi
