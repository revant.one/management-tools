import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import * as express from 'express';

async function bootstrap() {
  const server = express();
  server.use(express.static(join(process.cwd(), 'dist/client')));
  const app = await NestFactory.create(AppModule, server);
  await app.listen(9000);
}
bootstrap();
