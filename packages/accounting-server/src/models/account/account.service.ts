import { Injectable, BadRequestException } from '@nestjs/common';
import { Account } from './account.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { TreeRepository } from 'typeorm';
import { ACCOUNT_TYPES } from './../../constants/app-strings';
import * as uuidv4 from 'uuid/v4';
import { from } from 'rxjs';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account)
    private readonly accountRepository: TreeRepository<Account>,
  ) {}

  async find(limit, offset, search, sort, query) {
    let account;
    if (!search) {
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where(query)
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    } else {
      search = search;
      account = await this.accountRepository
        .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
        .orderBy(`"accountName"`, sort)
        .where('"accountName" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountNumber" LIKE :search', { search: `%${search}%` })
        .orWhere('"accountType" LIKE :search', { search: `%${search}%` })
        .skip(offset)
        .take(limit)
        .getMany();
      const out = {
        docs: account,
        offset,
        length: await this.accountRepository.count(),
      };

      return out;
    }
    // } else {
    //   account = await this.accountRepository
    //     .createQueryBuilder('account') // first argument is an alias. Alias is what you are selecting - photos. You must specify it.
    //     .orderBy(`"accountName"`, sort)
    //     .where(`( "accountName" = '${search}' )`)
    //     .skip(offset)
    //     .take(limit)
    //     .setParameters({ search })
    //     .getMany();
    //   return account;
    // }
  }

  async create(payload) {
    const account = new Account();
    const isRoot = false;
    const parent = await this.findParent(payload.parent);
    payload.parent = parent[0].id;
    if (this.validateAccount(payload.accountType, payload.parent)) {
      account.uuid = uuidv4();
      let queryString = 'INSERT INTO "account" ';
      queryString +=
        '("uuid", "accountNumber", "accountName", "accountType","isRoot", "isGroup", "parentId") ';
      queryString += 'VALUES ($1, $2, $3, $4, $5, $6, $7)';
      this.accountRepository.query(queryString, [
        account.uuid,
        payload.accountNumber,
        payload.accountName,
        payload.accountType,
        isRoot,
        payload.isGroup,
        payload.parent,
      ]);
    } else {
      throw new BadRequestException();
    }
    return { uuid: account.uuid };
  }

  async findOne(params): Promise<any> {
    if (params.uuid) {
      return await this.findAccountByUUID(params.uuid);
    } else {
      return await this.accountRepository.findOne(params);
    }
  }

  async findAccountByUUID(uuid) {
    const account = await this.accountRepository.findOne({ uuid });
    if (account.isRoot === true) {
      return account;
    }
    const parentId = await this.accountRepository.query(
      'SELECT "parentId" FROM account WHERE uuid = $1',
      [uuid],
    );

    const data = parentId.length > 0 ? parentId[0].parentId : null;
    if (!data) throw new BadRequestException();

    const parent = await this.accountRepository.query(
      'SELECT "accountNumber" FROM account WHERE id = $1',
      [data],
    );

    account.parent = parent.length > 0 ? parent[0].accountNumber : null;
    if (!account.parent) throw new BadRequestException();

    return account;
  }

  async findParent(parent) {
    return await this.accountRepository.query(
      'select id from account where "accountNumber" = $1',
      [parent],
    );
  }

  async delete(params) {
    // TODO: Only delete if Account not linked with
    // other Entities like GL, GLA, JEA

    const account = await this.findOne(params);
    if ((await account.children).length === 0) {
      this.accountRepository.query(
        'DELETE FROM account_closure WHERE uuid_ancestor = $1',
        [params.uuid],
      );
      return this.accountRepository.query('DELETE FROM account WHERE uuid = ', [
        params.uuid,
      ]);
    }
  }

  async update(payload) {
    await this.accountRepository.query(
      `UPDATE account
      SET "accountName" = $1
      WHERE "uuid" = $2`,
      [payload.accountName, payload.uuid],
    );
    return this.accountRepository.query(
      `SELECT * FROM account WHERE uuid ='${payload.uuid}'`,
    );
  }

  validateAccount(payloadAccountType, payloadAccountParent) {
    if (ACCOUNT_TYPES.includes(payloadAccountType) && payloadAccountParent) {
      return true;
    }
    return false;
  }

  getTree() {
    return from(this.accountRepository.findTrees());
  }

  getRepository() {
    return this.accountRepository;
  }
}
