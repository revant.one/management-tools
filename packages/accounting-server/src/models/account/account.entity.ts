import {
  Entity,
  Column,
  Tree,
  TreeParent,
  TreeChildren,
  OneToMany,
  PrimaryGeneratedColumn,
  Index,
  BaseEntity,
} from 'typeorm';
import { JournalEntryAccount } from '../journal-entry-account/journal-entry-account.entity';
import { GeneralLedger } from '../general-ledger/general-ledger.entity';

@Entity()
@Tree('closure-table')
@Index(['accountNumber', 'uuid'])
export class Account extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uuid: string;

  @Column({ unique: true })
  accountNumber: string;

  @Column()
  accountName: string;

  @Column()
  accountType: string;

  @Column({ type: 'boolean' })
  isGroup: boolean;

  @Column({ type: 'boolean' })
  isRoot: boolean;

  @OneToMany(type => JournalEntryAccount, entry => entry.account, {
    cascade: true,
  })
  journalEntryAccounts: JournalEntryAccount[];

  @OneToMany(type => GeneralLedger, generalLedger => generalLedger.account, {
    cascade: true,
  })
  generalLedgerAccounts: GeneralLedger[];

  @TreeParent()
  parent: Account;

  @TreeChildren()
  children: Account[];
}
