import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Settings } from './settings.collection';
import { settingsAlreadyExists } from '../../exceptions';

@Injectable()
export class SettingsService {
  constructor(
    @InjectRepository(Settings)
    private readonly SettingsRepository: Repository<Settings>,
  ) {}

  async save(params) {
    let serverSettings = new Settings();
    if (params.uuid) {
      const exists: number = await this.count();
      serverSettings = await this.findOne({ uuid: params.uuid });
      serverSettings.appURL = params.appURL;
      if (exists > 0 && !serverSettings) {
        throw settingsAlreadyExists;
      }
      serverSettings.save();
    } else {
      Object.assign(serverSettings, params);
    }
    return await this.SettingsRepository.save(serverSettings);
  }

  async find(): Promise<Settings> {
    const settings = await this.SettingsRepository.find();
    return settings.length ? settings[0] : null;
  }

  async findOne(params) {
    return await this.SettingsRepository.findOne(params);
  }

  async update(query, params) {
    return await this.SettingsRepository.update(query, params);
  }

  async count() {
    return this.SettingsRepository.count();
  }
}
