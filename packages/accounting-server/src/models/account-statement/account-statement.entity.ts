import {
  Entity,
  OneToOne,
  Column,
  UpdateDateColumn,
  JoinColumn,
  PrimaryColumn,
} from 'typeorm';
import { Account } from '../account/account.entity';

@Entity()
export class AccountStatement {
  @PrimaryColumn()
  uuid: string;

  @OneToOne(type => Account, { primary: true })
  @JoinColumn()
  account: Account;

  @Column({ type: 'float8' })
  closingBalance: number;

  @UpdateDateColumn()
  lastUpdated: Date;
}
