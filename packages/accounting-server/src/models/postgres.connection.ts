import { ConfigService } from '../config/config.service';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';
import { Account } from './account/account.entity';
import { AccountStatement } from './account-statement/account-statement.entity';
import { GeneralLedger } from './general-ledger/general-ledger.entity';
import { GeneralLedgerArchive } from './general-ledger-archive/general-ledger-archive.entity';
import { JournalEntry } from './journal-entry/journal-entry.entity';
import { JournalEntryAccount } from './journal-entry-account/journal-entry-account.entity';

const config = new ConfigService();

export const POSTGRES_CONNECTION: PostgresConnectionOptions = {
  name: 'postgres',
  type: 'postgres',
  username: config.get('POSTGRESDB_USER'),
  password: config.get('POSTGRESDB_PASSWORD'),
  host: config.get('POSTGRESDB_HOST'),
  database: config.get('POSTGRESDB_NAME'),
  logging: true,
  entities: [
    Account,
    AccountStatement,
    GeneralLedger,
    GeneralLedgerArchive,
    JournalEntry,
    JournalEntryAccount,
  ],
};
