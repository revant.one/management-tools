import {
  Module,
  HttpModule,
  NestModule,
  MiddlewareConsumer,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ModelsModule } from './models/models.module';
import { POSTGRES_CONNECTION } from './models/postgres.connection';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SetupController } from './controllers/setup/setup.controller';
import { SetupService } from './controllers/setup/setup.service';
import { ConnectController } from './controllers/microservice/connect.controller';
import { TokenGuard } from './gaurds/token.guard';
import { RoleGuard } from './gaurds/role.guard';
import { AuthServerVerificationGuard } from './gaurds/authserver-verification.guard';
import { AccountController } from './controllers/account/account.controller';
import { JournalEntryController } from './controllers/journal-entry/journal-entry.controller';
import { AccountService } from './models/account/account.service';
import { JournalEntryManagementService } from './controllers/journal-entry/journal-entry-management.service';
import { ConfigModule } from './config/config.module';
import { ServeAssetMiddleware } from './middlewares/serve-asset.middleware';
import { MONGODB_CONNECTION } from './models/monodb.connection';
import { CommonModule } from './common/common.module';
import { JournalEntryService } from './models/journal-entry/journal-entry.service';

@Module({
  imports: [
    ConfigModule,
    HttpModule,
    ModelsModule,
    CommonModule,
    TypeOrmModule.forRoot(POSTGRES_CONNECTION),
    TypeOrmModule.forRoot(MONGODB_CONNECTION),
    CommonModule,
  ],
  controllers: [
    AppController,
    SetupController,
    ConnectController,
    AccountController,
    JournalEntryController,
    AccountController,
  ],
  providers: [
    AppService,
    SetupService,
    AuthServerVerificationGuard,
    TokenGuard,
    RoleGuard,
    AccountService,
    JournalEntryManagementService,
    JournalEntryService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ServeAssetMiddleware).forRoutes('home*', 'tree*');
  }
}
