import { Injectable, BadRequestException } from '@nestjs/common';
import { AccountService } from '../../models/account/account.service';
import { getConnection } from 'typeorm';
import { JournalEntryService } from '../../models/journal-entry/journal-entry.service';
import { JournalEntryAccountService } from '../../models/journal-entry-account/journal-entry-account.service';
import { JournalEntry } from '../../models/journal-entry/journal-entry.entity';
import { GeneralLedgerService } from '../../models/general-ledger/general-ledger.service';

@Injectable()
export class JournalEntryManagementService {
  constructor(
    private journalEntryService: JournalEntryService,
    private journalEntryAccountService: JournalEntryAccountService,
    private accountService: AccountService,
    private glaService: GeneralLedgerService,
  ) {}

  async getAll(limit, offset, search, sort, query) {
    return await this.journalEntryService.getAll(
      limit,
      offset,
      search,
      sort,
      query,
    );
  }

  async getOne(id, limit, offset, search, sort, query) {
    const accounts = await this.journalEntryAccountService
      .getRepository()
      .query(
        `SELECT "entryType" , "amount" , "accountId" from journal_entry_account where "journalEntryTransactionId" = $1`,
        [id],
      );

    for (const account of accounts) {
      const accData = await this.accountService
        .getRepository()
        .query('SELECT "accountNumber" FROM "account" where id = $1', [
          account.accountId,
        ]);
      account.account = accData[0].accountNumber;
    }

    return accounts;
  }

  async create(payload) {
    let account;
    const journalEntry = await this.validateJE(payload);

    for (account of payload.accounts) {
      const index = await this.accountService.getRepository().query(
        `
      SELECT id FROM account WhERE "accountNumber" = $1
      `,
        [account.account],
      );
      await this.journalEntryAccountService
        .getRepository()
        .query(
          `insert into journal_entry_account ( "entryType","amount","journalEntryTransactionId","accountId") values ( $1 , $2 , $3 , $4)`,
          [
            account.entryType,
            account.amount,
            journalEntry.transactionId,
            index[0].id,
          ],
        );
      await this.glaService.getRepository().query(
        `insert into general_ledger ( "transactionDate" , "amountType","amount","journalEntryTransactionId","accountId")
          values ( $1 , $2 , $3 , $4 , $5)`,
        [
          journalEntry.transactionDate,
          account.entryType,
          account.amount,
          journalEntry.transactionId,
          index[0].id,
        ],
      );
    }

    return journalEntry.transactionId;
  }

  async validateJE(payload) {
    const param = [];
    let account,
      creditAmount = 0,
      debitAmount = 0;
    for (account of payload.accounts) {
      param.push(account.account);
      param.join(',');

      if (account.entryType === 'CREDIT') {
        debitAmount += account.amount;
      } else {
        creditAmount += account.amount;
      }
    }
    if (creditAmount === 0 || debitAmount === 0) {
      throw new BadRequestException();
    }
    if (debitAmount !== creditAmount) {
      throw new BadRequestException();
    }

    for (const iterator of param) {
      const response = await this.accountService
        .getRepository()
        .query(
          `SELECT uuid FROM account WHERE "accountNumber" = $1 and "isGroup" = false `,
          [iterator],
        );
      if (response.length === 0) {
        throw new BadRequestException();
      }
    }

    const journalEntry = new JournalEntry();
    const id = await getConnection('postgres')
      .getRepository(JournalEntry)
      .save(journalEntry);
    return id;
  }
}
