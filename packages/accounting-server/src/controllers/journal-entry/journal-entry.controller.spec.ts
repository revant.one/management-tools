import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryController } from './journal-entry.controller';
import { JournalEntryManagementService } from './journal-entry-management.service';
import { AuthServerVerificationGuard } from '../../gaurds/authserver-verification.guard';
import { TokenGuard } from '../../gaurds/token.guard';

describe('JournalEntry Controller', () => {
  let module: TestingModule;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [JournalEntryController],
      providers: [
        {
          provide: JournalEntryManagementService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(AuthServerVerificationGuard)
      .useValue({})
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();
  });
  it('should be defined', () => {
    const controller: JournalEntryController = module.get<
      JournalEntryController
    >(JournalEntryController);
    expect(controller).toBeDefined();
  });
});
