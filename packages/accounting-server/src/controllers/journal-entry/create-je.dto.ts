import { JournalEntryAccountDto } from '../../models/journal-entry-account/journal-entry-account.dto';
import { IsNotEmpty } from 'class-validator';

export class DtoCreateJE {
  @IsNotEmpty()
  accounts: JournalEntryAccountDto[];
}
