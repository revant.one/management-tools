import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { TokenCacheService } from '../../models/token-cache/token-cache.service';
import { AuthServerVerificationGuard } from '../../gaurds/authserver-verification.guard';
import { USER_ALLREADY_EXIST } from './../../constants/messages';
import { TokenGuard } from '../../gaurds/token.guard';

@Controller('connect')
@UseGuards(TokenGuard)
export class ConnectController {
  constructor(private readonly tokenCacheService: TokenCacheService) {}

  @Post('v1/token_delete')
  @UseGuards(AuthServerVerificationGuard)
  async tokenDelete(@Body('accessToken') accessToken) {
    await this.tokenCacheService.deleteMany({ accessToken });
  }

  @Post('v1/user_delete')
  @UseGuards(AuthServerVerificationGuard)
  async userDelete(@Body('user') user) {
    throw USER_ALLREADY_EXIST;
  }
}
