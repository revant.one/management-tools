import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  Req,
  Query,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AccountService } from '../../models/account/account.service';
import { AccountDto } from './account-dto';
import { INDEX_HTML } from '../../constants/filesystem';
import { TokenGuard } from '../../gaurds/token.guard';

@Controller('account')
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  async createAccount(@Body() payload) {
    return await this.accountService.create(payload);
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  async list(
    @Req() req,
    @Query('limit') limit: number = 10,
    @Query('offset') offset: number = 0,
    @Query('search') search?: string,
    @Query('sort') sort?: string,
    @Query('filters') filters?,
  ) {
    // filters = [
    //   ["date", ">", "2000"],
    //   ["date", ">", "1999"],
    //   ["isGroup", "=", true],
    // ]
    // filters.forEach(condition => {
    //    condition.forEach( element => {
    //      if(element in OPERATORS){
    //        let query  = " '" + element + "' ";
    //        console.log(query);

    //       }
    //       else{
    //         let query = element;
    //         console.log(query);
    //       }

    //    });
    // });

    // if (!(await this.userService.checkAdministrator(req.user.user))) {
    //   query.createdBy = req.user.utser;
    // }

    sort = sort ? sort.toUpperCase() : 'ASC';

    return this.accountService.find(limit, offset, search, sort, filters);
  }

  @Put('v1/update')
  @UseGuards(TokenGuard)
  async updateAccount(@Body() payload) {
    return await this.accountService.update(payload);
  }

  @Delete('v1/delete/:uuid')
  @UseGuards(TokenGuard)
  async deleteAccount(@Param('uuid') uuid) {
    return await this.accountService.delete({ uuid });
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getAccount(@Param('uuid') uuid: AccountDto) {
    return await this.accountService.findOne({ uuid });
  }

  @Get('v1/tree')
  @UseGuards(TokenGuard)
  getTree() {
    return this.accountService.getTree();
  }

  @Get()
  root(@Res() res) {
    res.sendFile(INDEX_HTML);
  }

  @Get('*')
  wildcard(@Res() res) {
    res.sendFile(INDEX_HTML);
  }
}
