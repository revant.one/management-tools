import { Injectable, MiddlewareFunction, NestMiddleware } from '@nestjs/common';
import { INDEX_HTML } from '../constants/filesystem';

@Injectable()
export class ServeAssetMiddleware implements NestMiddleware {
  resolve(...args: any[]): MiddlewareFunction {
    return (req, res) => {
      res.sendFile(INDEX_HTML);
    };
  }
}
