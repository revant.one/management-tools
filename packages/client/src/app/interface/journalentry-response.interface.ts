export interface CreateJeRespose {
  entryType: string;
  amount: Float32Array;
  account: string;
  journalEntryTransactionId: number;
}
