import { Injectable } from '@angular/core';
import { APP_URL } from '../constants/storage';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root',
})
export class FormService {
  constructor(
    private readonly storageService: StorageService,
    private http: HttpClient,
    private authService: AuthService,
  ) {}

  getAccount(model, uuid) {
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/get/${uuid}`;
    return this.http.get(url, { headers: this.authService.headers });
  }
  deleteAccount(model, uuid) {
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/delete/${uuid}`;
    return this.http.delete(url, { headers: this.authService.headers });
  }

  createAccount(
    model: string,
    accountNumber: string,
    accountName: string,
    accountType: string,
    isGroup: boolean,
    parent: string,
  ) {
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/create`;

    const accountData = {
      accountNumber,
      accountName,
      accountType,
      isGroup,
      parent,
    };
    return this.http.post(url, accountData, {
      headers: this.authService.headers,
    });
  }

  updateAccount(uuid, model, accountName, accountNumber) {
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/update`;

    const accountData = {
      uuid,
      accountName,
      accountNumber,
    };
    return this.http.put(url, accountData, {
      headers: this.authService.headers,
    });
  }

  getJe(model, journalEntryTransactionId) {
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/je/create`;

    const payload = {
      journalEntryTransactionId,
    };

    return this.http.post(url, payload, { headers: this.authService.headers });
  }
}
