import { Injectable } from '@angular/core';
import { APP_URL } from '../constants/storage';
import { StorageService } from '../common/storage.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../common/auth.service';

@Injectable({
  providedIn: 'root',
})
export class JournalEntryService {
  constructor(
    private readonly storageService: StorageService,
    private readonly http: HttpClient,
    private readonly oauthService: AuthService,
  ) {}

  getJe(model, journalEntryTransactionId) {
    model = 'journalentry';
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/getOne/${journalEntryTransactionId}`;
    // TODO: Interface for je response;
    return this.http.get<any>(url, { headers: this.oauthService.headers });
  }

  createJe(rows) {
    const model = 'journalentry';
    const issuer = this.storageService.getInfo(APP_URL);
    const url = `${issuer}/${model}/v1/create`;
    return this.http.post(
      url,
      { accounts: rows },
      { headers: this.oauthService.headers },
    );
  }
}
