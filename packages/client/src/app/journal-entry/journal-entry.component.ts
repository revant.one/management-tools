import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { ENTRY_TYPE } from '../constants/storage';
import { JournalEntryService } from './journal-entry.service';
import { ListingService } from '../common/listing.service';
import { CreateJeResponse } from './journal-entry.interface';
import {
  MatSnackBar,
  MatTableDataSource,
  MatPaginator,
  MatSort,
} from '@angular/material';

@Component({
  selector: 'app-journalentry',
  templateUrl: './journal-entry.component.html',
  styleUrls: ['./journal-entry.component.css'],
})
export class JournalEntryComponent implements OnInit {
  uuid: string;
  model: string;
  entryType: string;
  amount: number;
  account: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  journalEntryTransactionId: number;
  types = ENTRY_TYPE;
  filter: string;
  sortOrder: 'ASC' | 'DSC';
  pageNumber: number;
  pageSize: number;
  query: string;
  displayedColumns = ['account', 'entryType', 'amount', 'button'];
  ELEMENT_DATA = [
    {
      account: '',
      entryType: 'CREDIT',
      amount: 0,
    },
  ];
  dataSource = new MatTableDataSource(this.ELEMENT_DATA);

  jeForm: FormGroup;
  editing = {};
  responseData: any[] = [];
  data;
  dropDownData: any[] | any;
  createJeDto: CreateJeResponse;

  constructor(
    private journalEntryService: JournalEntryService,
    private router: Router,
    private snackbar: MatSnackBar,
    private accountService: ListingService,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder, // private snackbar: MatSnackBar,
  ) {
    // TODO: get data from server

    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[1];
      });
  }

  ngOnInit() {
    if (this.activatedRoute.snapshot.params.id !== 'new') {
      this.journalEntryTransactionId = this.activatedRoute.snapshot.params.id;
    }

    this.dropDownData = this.accountService
      .findModels(
        (this.model = 'account'),
        this.filter,
        this.sortOrder,
        this.pageNumber,
        this.pageSize,
        (this.query = `"isGroup" = 'false'`),
      )
      .pipe(
        map((data: any) => {
          return data.docs;
        }),
      );

    if (this.journalEntryTransactionId) {
      this.dataSource.data = this.responseData;
    }

    this.jeForm = this.formBuilder.group({
      entryType: '',
      amount: 0,
      account: 1,
    });

    if (this.journalEntryTransactionId) {
      this.journalEntryService
        .getJe(this.model, this.journalEntryTransactionId)
        .subscribe({
          next: response => {
            this.dataSource.data = response;
            this.dataSource._updateChangeSubscription();
          },
        });
    }
  }
  removeRow(index) {
    this.dataSource.data.splice(index, 1);
    this.dataSource._updateChangeSubscription();
  }

  addRow() {
    this.ELEMENT_DATA.push({
      account: '',
      entryType: '',
      amount: 0,
    });

    this.dataSource._updateChangeSubscription();
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  createJe() {
    this.journalEntryService.createJe(this.dataSource.data).subscribe({
      next: res => {
        if (res) {
          this.snackbar.open('Journal Entry Created', 'Close', {
            duration: 2500,
          });
          this.router.navigateByUrl('/journalentry');
        }
      },
      error: err => {
        this.snackbar.open('err', 'Close', { duration: 2500 });
      },
    });
  }
}
