/**
 * Json node data with nested structure. Each node has a filename and a value or a list of children
 */
export class TreeNode {
  id: number;
  uuid: string;
  accountNumber: string;
  accountName: string;
  accountType: string;
  isGroup: boolean;
  children: TreeNode[];
}
