import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TreeNode } from './tree.node';
import { TreeService } from './tree.service';

@Injectable()
export class TreeDatabase {
  dataChange = new BehaviorSubject<TreeNode[]>([]);

  get data(): TreeNode[] {
    return this.dataChange.value;
  }

  constructor(private model: string, private treeService: TreeService) {
    this.initialize();
  }

  initialize() {
    this.treeService.getTree(this.model).subscribe({
      next: response => {
        const data = response;
        // Notify the change.
        this.dataChange.next(data);
      },
      error: err => {
        // handle error
      },
    });
  }
}
