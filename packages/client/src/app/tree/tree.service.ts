import { Injectable } from '@angular/core';
import { AuthService } from '../common/auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../common/storage.service';
import { APP_URL } from '../constants/storage';
import { TreeNode } from './tree.node';

@Injectable({
  providedIn: 'root',
})
export class TreeService {
  constructor(
    private authService: AuthService,
    private http: HttpClient,
    private storageService: StorageService,
  ) {}

  getTree(model) {
    const appUrl = this.storageService.getInfo(APP_URL);
    return this.http.get<TreeNode[]>(`${appUrl}/${model}/v1/tree`, {
      headers: this.authService.headers,
    });
  }
}
