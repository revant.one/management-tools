import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { TreeDatabase } from './tree.datasource';
import { TreeNode } from './tree.node';
import { TreeService } from './tree.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent implements OnInit {
  nestedTreeControl: NestedTreeControl<TreeNode>;
  nestedDataSource: MatTreeNestedDataSource<TreeNode>;
  database: TreeDatabase;
  model: string;

  constructor(private router: Router, private treeService: TreeService) {
    this.router.events
      .pipe(filter(route => route instanceof NavigationEnd))
      .subscribe((route: NavigationEnd) => {
        this.model = route.url.split('/')[2];
      });
  }

  ngOnInit() {
    this.database = new TreeDatabase(this.model, this.treeService);
    this.nestedTreeControl = new NestedTreeControl<TreeNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();
    this.database.dataChange.subscribe(
      data => (this.nestedDataSource.data = data),
    );
  }

  hasNestedChild = (_: number, nodeData: TreeNode) => !nodeData.accountType;

  private _getChildren = (node: TreeNode) => node.children;
}
