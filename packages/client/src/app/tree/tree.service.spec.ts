import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TreeService } from './tree.service';
import { AuthService } from '../common/auth.service';
import { StorageService } from '../common/storage.service';

describe('TreeService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: AuthService,
          useValue: {},
        },
        {
          provide: StorageService,
          useValue: {},
        },
      ],
    }),
  );

  it('should be created', () => {
    const service: TreeService = TestBed.get(TreeService);
    expect(service).toBeTruthy();
  });
});
